package nl.utwente.di.calculator;


import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class Calculator extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Converter Converter;

    public void init() throws ServletException {
        Converter = new Converter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Calculator";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temp in C: " +
                request.getParameter("tempC") + "\n" +
                "  <P>Temp in F: "
                + Double.toString(Converter.getFarhenheit(request.getParameter("tempC"))) +
                "</BODY></HTML>");
    }


}
