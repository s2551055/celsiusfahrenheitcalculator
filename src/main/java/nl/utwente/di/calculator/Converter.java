package nl.utwente.di.calculator;

public class Converter {
    public double getFarhenheit(String tempC) {
        double temp = Double.parseDouble(tempC);
        return 1.8 * temp + 32;
    }
}
